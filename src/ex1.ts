function add(a : number, b : number) : number {
    return a + b;
}

function concat(a : string, b : string) : string {
    return `${a}${b}`;
}

const resultNumber = add(25, 26);
const resultString = concat('Albert', 'Einstein');

console.log({resultNumber, resultString});