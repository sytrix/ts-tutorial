function last(element : string[]) : string {
    return element[element.length - 1];
}

const array1 = ['Mary Poppins', 'Bart Simpson', 'King Kong'];
const array2 = [23, 5, 12, 8, 16];
const array3 : string[] = [];

const res1 = last(array1);

console.log('-> ', res1);


