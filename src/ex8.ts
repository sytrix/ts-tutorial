type ArrayNode = {
    next?: ArrayNode,
    value: string,
};


const nodesString : ArrayNode = {
    value: "Chocolat",
    next: {
        value: "Chaussure",
        next: {
            value: "Chocobon",
            next: {
                value: "Nutella",
            }
        }
    }
}

/*
const nodesNumber : ArrayNode = {
    value: 1024,
    next: {
        value: 512,
        next: {
            value: 15,
            next: {
                value: "choco",
            }
        }
    }
}


const nodesObject : ArrayNode = {
    value: [],
    next: {
        value: [],
        next: {
            value: [],
            next: {
                value: [],
            }
        }
    }
}
*/