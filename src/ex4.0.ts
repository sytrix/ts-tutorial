type StringOrNumber = string|number;
type Binary = 0|1;

function binToBool(b : Binary) : boolean {
    return b === 1;
}


