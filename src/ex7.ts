type CallbackFunction = (index:number)=>void;

function nExecution(n : number, callback : CallableFunction) : void {
    for (let i = 0; i < n; i++) {
        callback(i);
    }
}

nExecution(5, (index:number) => {
    console.log(`Hello ${index}`);
})