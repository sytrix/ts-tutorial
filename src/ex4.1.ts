type Animal = 'Lion'|'Tiger'|'Cat';
type Family = 'Felidae';

function animalFamily(animal : Animal) : Family {
    switch(animal) {
        case 'Cat':
        case 'Lion':
        case 'Tiger':
            return 'Felidae';
    }
}


