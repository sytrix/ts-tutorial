type User = {
    firstName?: string,
    lastName?: string,
    nickName: string,
    species: 'Human'|'Program'
};

const users : User[] = [
    {
        firstName: 'Thomas',
        lastName: 'Anderson',
        nickName: 'The One',
        species: 'Human',
    },
    {
        nickName: 'Agent Smith',
        species: 'Program',
    }
]

console.log({users});


