type SwordBase = {
    length: number,
}

type LightSaber = SwordBase & {
    type: 'Light Saber', 
    color: 'Red'|'Green'|'Blue'|'Purple',
};
type NinjaSaber = SwordBase & {
    type: 'Ninja Saber',
    subtype: 'Katana'|'Tachi'|'Kodachi'|'Ninjato'|'Nagamaki'
};
type MinecraftSword = SwordBase & {
    type: 'Minecraft Sword',
    material: 'Diamond'|'Iron'|'Gold'|'Stone'|'Wood',
};

type Sword = LightSaber | NinjaSaber | MinecraftSword;

const maceWinduSword : Sword = {type:'Light Saber', length:0.91+0.27, color:'Purple'};
const musashiMiyamotoSword : Sword = {type:'Ninja Saber', length:0.52, subtype:'Kodachi'};
const aRandomMinecraftPlayerSword : Sword = {type:'Minecraft Sword', length:1.2, material:'Stone'};
